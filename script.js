// Variables
const userOutputElement = document.querySelector("#userOutput");
const userNameInput = document.querySelector("#userNameInput");
const userIdInput = document.querySelector("#userIdInput");
const userNameInputBtn = document.querySelector("#userNameInputBtn");
const userIdInputBtn = document.querySelector("#userIdInputBtn");



// Functions
function getUser() {
    // Listener by name
    userNameInputBtn.addEventListener("click", searchUserByName);

    // Listener by id
    userIdInputBtn.addEventListener("click", searchUserById);
}

function userOutput(user) {
    const ul = document.createElement("ul");
    userOutputElement.appendChild(ul);

    for (let i = 0; i < Object.keys(user).length; i++) {
        const li = document.createElement("li");
        ul.appendChild(li);
        li.innerHTML = `${Object.keys(user)[i]} - ${Object.values(user)[i]}`;
    }
}

function searchUserByName() {
    clearElem(userOutputElement);

    const searchUserName = userNameInput.value;
    const separateFirstAndLastName = searchUserName.split(" ");

    async function searchUsersOnServer() {
        try {
            const serverRequest = await fetch("people.json");
            const dataProcessing = await serverRequest.json();

            const allUsers = await dataProcessing.users;

            for(let user of allUsers) {
                const userName = user.name;
                const userLastName = user.lastName

                if (
                    separateFirstAndLastName[0] === userName &&
                    separateFirstAndLastName[1] === userLastName
                ) {
                    userOutput(user);
                }
            }
        } catch(err) {
            alert("Something went wrong");
        }
    }

    searchUsersOnServer()
}

function searchUserById() {
    clearElem(userOutputElement);

    const searchUserId = +userIdInput.value;

    async function searchUsersOnServer() {
        try {
            const serverRequest = await fetch("people.json");
            const dataProcessing = await serverRequest.json();

            const allUsers = await dataProcessing.users;

            for(let user of allUsers) {
                const userId = +user.id;

                if (searchUserId === userId) {
                    userOutput(user);
                } 
            }
        } catch (err) {
            alert("Something went wrong");
        }
    }

    searchUsersOnServer()
}

function clearElem(elem) {
    elem.innerHTML = "";
}


getUser()